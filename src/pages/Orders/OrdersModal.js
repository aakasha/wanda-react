import React from "react"
import PropTypes from "prop-types"
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap"
import * as ls from "local-storage";

const OrdersModal = props => {

  const { isOpen, toggle } = props;
  const { orderdate, toAddress, fromAddress, createdAt, orderstatus, 
          email, name, badgeclass, service,
          note, movingDate,updatedAt, id, phone
        } = ls.get('orderDatail');

  const handleServices = services => {
    if (Array.isArray(services))
      return services.join(", ");
    return "";
  };
  return (
    <Modal
      isOpen={isOpen}
      role="dialog"
      autoFocus={true}
      centered={true}
      className="exampleModal"
      tabIndex="-1"
      toggle={toggle}
    >
      <div className="modal-content">
        <ModalHeader toggle={toggle}>Order Details</ModalHeader>
        <ModalBody>
 

          <div className="mb-4">
            ID: <span className="text-primary">{id}</span>
          </div>
          <div className="mb-4">
            name: <span className="text-primary">{name}</span>
          </div>
          <div className="mb-4">
            email: <span className="text-primary">{email}</span>
          </div>
          <div className="mb-4">
            Phone: <span className="text-primary">{phone}</span>
          </div>
          <div className="mb-4">
            Moving Date: <span className="text-primary">{movingDate}</span>
          </div>
          <div className="mb-4">
            service: <span className="text-primary">{handleServices(service)}</span>
          </div>
          <div className="mb-4">
            To (Address): <span className="text-primary">{toAddress}</span>
          </div>
          <div className="mb-4">
            From (Address): <span className="text-primary">{fromAddress}</span>
          </div>
          <div className="mb-4">
            Order Date: <span className="text-primary">{orderdate}</span>
          </div>
          <div className="mb-4">
            Status: <span className="text-primary">{orderstatus}</span>
          </div>
          <div className="mb-4">
            Note: <span className="text-primary">{note}</span>
          </div>
          <div className="mb-4">
            Badge: <span className="text-primary">{badgeclass}</span>
          </div>
          <div className="mb-4">
            Created At: <span className="text-primary">{createdAt}</span>
          </div>
          <div className="mb-4">
            Updated At: <span className="text-primary">{updatedAt}</span>
          </div>

        </ModalBody>
        <ModalFooter>
          <Button type="button" color="secondary" onClick={toggle}>
            Close
          </Button>
        </ModalFooter>
      </div>
    </Modal>
  )
}

OrdersModal.propTypes = {
  toggle: PropTypes.func,
  isOpen: PropTypes.bool,
}

export default OrdersModal
