import React, { useEffect, useState, useRef } from "react";
import MetaTags from "react-meta-tags";
import PropTypes from "prop-types";
import { withRouter, Link } from "react-router-dom";
import { isEmpty } from "lodash";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationListStandalone,
  PaginationProvider,
} from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import * as moment from "moment";
import * as ls from "local-storage";


import {
  Button,
  Card,
  CardBody,
  Col,
  Container,
  Row,
  Badge,
  UncontrolledTooltip,
  Modal,
  ModalHeader,
  ModalBody,
} from "reactstrap";
import { AvForm, AvField } from "availity-reactstrap-validation";

//redux
import { useSelector, useDispatch } from "react-redux";

//Import Breadcrumb
import Breadcrumbs from "components/Common/Breadcrumb";

import {
  getOrders as onGetOrders,
  addNewOrder as onAddNewOrder,
  updateOrder as onUpdateOrder,
  deleteOrder as onDeleteOrder,
} from "store/actions";

import OrdersModal from "./OrdersModal";

const Orders = props => {
  const dispatch = useDispatch();

  const { orders } = useSelector(state => ({
    orders: state.Order.orders,
  }));

  const selectRow = {
    mode: "checkbox",
  };

  const [modal, setModal] = useState(false);
  const [modal1, setModal1] = useState(false);
  const [orderList, setOrderList] = useState([]);
  const [isEdit, setIsEdit] = useState(false);

  //pagination customization
  const pageOptions = {
    sizePerPage: 10,
    totalSize: orders.length, // replace later with size(orders),
    custom: true,
  };
  const { SearchBar } = Search;

  // const toggleModal = () => {
  //   setModal1(!modal1)
  // }
  const toggleViewModal = () => setModal1(!modal1);

  const OrderColumns = toggleModal => [
    {
      dataField: "id",
      text: "Order ID",
      sort: true,
      hidden: true,
      // eslint-disable-next-line react/display-name
      formatter: (cellContent, row) => (
        <Link to="#" className="text-body fw-bold" onClick={() => setOrderDatailAndToggleViewModal(row)}>
          {row.id}
        </Link>
      ),
    },
    {
      dataField: "createdAt",
      text: "Created At",
      sort: true,
      hidden: true,
    },
    {
      dataField: "name",
      text: "Name",
      sort: true,
    },
    {
      dataField: "phone",
      text: "Phone",
      sort: true,
    },
    {
      dataField: "email",
      text: "Email",
      sort: true,
    },
    // {
    //   dataField: "fromAddress",
    //   text: "From (Address)",
    //   sort: true,
    // },
    // {
    //   dataField: "toAddress",
    //   text: "To (Address)",
    //   sort: true,
    // },
    {
      dataField: "service",
      text: "Service(s)",
      sort: true,
      // eslint-disable-next-line react/display-name
      formatter: (cellContent, row) => handleServices(row.service),
    },
    {
      dataField: "movingDate",
      text: "Moving Date",
      sort: true,
      // eslint-disable-next-line react/display-name
      formatter: (cellContent, row) => handleValidDate(row.movingDate),
    },
    {
      dataField: "orderstatus",
      text: "Prder Status",
      sort: true,
      // eslint-disable-next-line react/display-name
      formatter: (cellContent, row) => (
        <Badge
          className={"font-size-12 badge-soft-" + row.badgeclass}
          color={row.badgeClass}
          pill
        >
          {row.orderstatus}
        </Badge>
      ),
    },
    {
      dataField: "view",
      isDummyField: true,
      text: "View Details",
      sort: true,
      // eslint-disable-next-line react/display-name
      formatter: (cellContent, order) => (
        <Button
          type="button"
          color="primary"
          className="btn-sm btn-rounded"
          onClick={() => setOrderDatailAndToggleViewModal(order)}
        >
          View Details
        </Button>
      ),
    },
    {
      dataField: "action",
      isDummyField: true,
      text: "Action",
      // eslint-disable-next-line react/display-name
      formatter: (cellContent, order) => (
        <>
          <div className="d-flex gap-3">
            <Link
              to="#"
              className="text-success"
              onClick={() => handleOrderClick(order)}
            >
              <i className="mdi mdi-pencil font-size-18" id="edittooltip" />
              <UncontrolledTooltip placement="top" target="edittooltip">
                Edit
              </UncontrolledTooltip>
            </Link>
            <Link
              to="#"
              className="text-danger"
              onClick={() => handleDeleteOrder(order)}
            >
              <i className="mdi mdi-delete font-size-18" id="deletetooltip" />
              <UncontrolledTooltip placement="top" target="deletetooltip">
                Delete
              </UncontrolledTooltip>
            </Link>
          </div>
        </>
      ),
    },
  ];

  useEffect(() => {
    if (orders && !orders.length) {
      dispatch(onGetOrders());
    }
  }, [dispatch, orders]);

  useEffect(() => {
    setOrderList(orders);
  }, [orders]);

  useEffect(() => {
    if (!isEmpty(orders) && !!isEdit) {
      setOrderList(orders);
      setIsEdit(false);
    }
  }, [orders]);

  const toggle = () => {
    setModal(!modal);
  };

  const toLowerCase1 = str => {
    return str.toLowerCase();
  };

  const handleOrderClick = arg => {
    const order = arg;

    setOrderList({
      id: order.id,
      name: order.name,
      phone: order.phone,
      email: order.email,
      fromAddress: order.fromAddress,
      toAddress: order.toAddress,
      service: order.service,
      movingDate: order.movingDate,
      orderstatus: order.orderstatus,
      badgeclass: order.badgeclass,
      service: order.service,
      note: order.note,

    });

    setIsEdit(true);

    toggle();
  };

  var node = useRef();
  const onPaginationPageChange = page => {
    if (
      node &&
      node.current &&
      node.current.props &&
      node.current.props.pagination &&
      node.current.props.pagination.options
    ) {
      node.current.props.pagination.options.onPageChange(page);
    }
  };

  const handleDeleteOrder = order => {
    if (order.id !== undefined) {
      dispatch(onDeleteOrder(order));
      onPaginationPageChange(1);
    }
  };

  const handleValidOrderSubmit = (e, values) => {
    if (isEdit) {
      const updateOrder = {
        id: orderList.id,
        name: values.name,
        phone: values.phone,
        email: values.email,
        fromAddress: values.fromAddress,
        toAddress: values.toAddress,
        service: values.service,
        movingDate: values.movingDate,
        orderstatus: values.orderstatus,
        badgeclass: values.badgeclass,
        service: values.service,
        note: values.note,
      };

      // update order
      dispatch(onUpdateOrder(updateOrder));
    } else {
      const newOrder = {
        id: "",
        name: values.name,
        phone: values.phone,
        email: values.email,
        fromAddress: values.fromAddress,
        toAddress: values.toAddress,
        service: values.service,
        movingDate: values.movingDate,
        orderstatus: values.orderstatus,
        badgeclass: values.badgeclass,
        service: values.service,
        note: values.note,
      };
      // save new order
      dispatch(onAddNewOrder(newOrder));
    }
    toggle();
  };

  if (!ls.get('orderDatail')) {
    const data = {
      orderdate: "",
      toAddress: "",
      fromAddress: "",
      createdAt: "",
      orderstatus: "",
      email: "",
      name: "",
      badgeclass: "",
      service: [],
      note: "",
      movingDate: "",
      updatedAt: "",
      id: "",
      phone: ""
    };
    ls.set('orderDatail', data)
  }

  const handleOrderClicks = () => {
    setOrderList("");
    setIsEdit(false);
    toggle();
  };

  const handleValidDate = date => {
    const date1 = moment(new Date(date)).format("DD MMM Y");
    return date1;
  };

  const handleServices = services => {
    return services.join(", ");
  };

  const setOrderDatailAndToggleViewModal = data => {
    ls.set('orderDatail', data)
    toggleViewModal();
  }

  const defaultSorted = [
    {
      dataField: "createdAt",
      order: "desc",
    },
  ];

  return (
    <React.Fragment>
      <OrdersModal isOpen={modal1} toggle={toggleViewModal} />
      <div className="page-content">
        <MetaTags>
          <title>Orders | Wanda - Sales Portal</title>
        </MetaTags>
        <Container fluid>
          <Breadcrumbs title="Wanda Sales Portal" breadcrumbItem="Orders" />
          <Row>
            <Col xs="12">
              <Card>
                <CardBody>
                  <PaginationProvider
                    pagination={paginationFactory(pageOptions)}
                    keyField="id"
                    columns={OrderColumns(toggle)}
                    data={orders}
                  >
                    {({ paginationProps, paginationTableProps }) => (
                      <ToolkitProvider
                        keyField="id"
                        data={orders}
                        columns={OrderColumns(toggle)}
                        bootstrap4
                        search
                      >
                        {toolkitProps => (
                          <React.Fragment>
                            <Row className="mb-2">
                              <Col sm="4">
                                <div className="search-box me-2 mb-2 d-inline-block">
                                  <div className="position-relative">
                                    <SearchBar {...toolkitProps.searchProps} />
                                    <i className="bx bx-search-alt search-icon" />
                                  </div>
                                </div>
                              </Col>
                              <Col sm="8">
                                <div className="text-sm-end">
                                  <Button
                                    type="button"
                                    color="success"
                                    className="btn-rounded  mb-2 me-2"
                                    onClick={handleOrderClicks}
                                  >
                                    <i className="mdi mdi-plus me-1" />
                                    Add New Order
                                  </Button>
                                </div>
                              </Col>
                            </Row>
                            <Row>
                              <Col xl="12">
                                <div className="table-responsive">
                                  <BootstrapTable
                                    keyField="id"
                                    responsive
                                    bordered={false}
                                    striped={false}
                                    defaultSorted={defaultSorted}
                                    selectRow={selectRow}
                                    classes={
                                      "table align-middle table-nowrap table-check"
                                    }
                                    headerWrapperClasses={"table-light"}
                                    {...toolkitProps.baseProps}
                                    {...paginationTableProps}
                                    ref={node}
                                  />
                                </div>
                                <Modal isOpen={modal} toggle={toggle}>
                                  <ModalHeader toggle={toggle} tag="h4">
                                    {!!isEdit ? "Edit Order" : "Add Order"}
                                  </ModalHeader>
                                  <ModalBody>
                                    <AvForm
                                      onValidSubmit={handleValidOrderSubmit}
                                    >
                                      <Row form>
                                        <Col className="col-12">

                                          {/* <div className="mb-3">
                                            <AvField
                                              name="orderId"
                                              label="Order Id"
                                              type="text"
                                              errorMessage="Invalid orderId"
                                              validate={{
                                                required: { value: true },
                                              }}
                                              value={orderList.id || ""}
                                              disabled
                                            />
                                          </div>

                                          <div className="mb-3">
                                            <AvField
                                              name="orderDate"
                                              label="Order Date"
                                              type="date"
                                              errorMessage="Invalid Date"
                                              validate={{
                                                required: { value: true },
                                              }}
                                              value={orderList.orderdate || ""}
                                              disabled
                                            />
                                          </div> */}

                                          <div className="mb-3">
                                            <AvField
                                              name="name"
                                              label="Name"
                                              type="text"
                                              errorMessage="Invalid Billing Name"
                                              validate={{
                                                required: { value: true },
                                              }}
                                              value={
                                                orderList.name || ""
                                              }
                                            />
                                          </div>

                                          <div className="mb-3">
                                            <AvField
                                              name="phone"
                                              label="phone"
                                              type="text"
                                              errorMessage="Invalid Phone"
                                              validate={{
                                                required: { value: true },
                                              }}
                                              value={orderList.phone || ""}
                                            />
                                          </div>

                                          <div className="mb-3">
                                            <AvField
                                              name="email"
                                              label="Email"
                                              type="text"
                                              errorMessage="Invalid Email"
                                              validate={{
                                                required: { value: true },
                                              }}
                                              value={orderList.email || ""}
                                            />
                                          </div>

                                          <div className="mb-3">
                                            <AvField
                                              name="fromAddress"
                                              label="From (Address)"
                                              type="text"
                                              errorMessage="Invalid Address"
                                              validate={{
                                                required: { value: true },
                                              }}
                                              value={orderList.fromAddress || ""}
                                            />
                                          </div>

                                          <div className="mb-3">
                                            <AvField
                                              name="toAddress"
                                              label="To (Address)"
                                              type="text"
                                              errorMessage="Invalid Address"
                                              validate={{
                                                required: { value: true },
                                              }}
                                              value={orderList.toAddress || ""}
                                            />
                                          </div>

                                          <div className="mb-3">
                                            <AvField
                                              name="service"
                                              label="Services"
                                              type="select"
                                              className="form-select"
                                              errorMessage="Invalid Services"
                                              validate={{
                                                required: { value: true },
                                              }}
                                              value={
                                                orderList.service || ""
                                              }
                                              multiple>
                                              <option value="wrap" key="wrap">Wrapping large items</option>
                                              <option value="pack" key="pack">Packing items into boxes</option>
                                              <option value="pickup" key="pickup">Express pickup (same-day pickup)</option>
                                            </AvField>
                                          </div>

                                          <div className="mb-3">
                                            <AvField
                                              name="movingDate"
                                              label="Moving Date"
                                              type="date"
                                              errorMessage="Invalid Date"
                                              validate={{
                                                required: { value: true },
                                              }}
                                              value={orderList.movingDate || ""}
                                            />
                                          </div>
                                          
                                          <div className="mb-3">
                                            <AvField
                                              name="orderstatus"
                                              label="Status"
                                              type="select"
                                              className="form-select"
                                              errorMessage="Invalid Status"
                                              validate={{
                                                required: { value: true },
                                              }}
                                              value={
                                                orderList.orderstatus || "pending"
                                              }
                                            >
                                              <option value="pending" key="pending">Pending</option>
                                              <option value="partial" key="partial">Partial</option>
                                              <option value="fulfilled" key="fulfilled">Fulfilled</option>
                                              <option value="failed" key="failed">Failed</option>
                                            </AvField>
                                          </div>

                                          <div className="mb-3">
                                            <AvField
                                              name="badgeclass"
                                              label="Badge Class"
                                              type="select"
                                              className="form-select"
                                              errorMessage="Invalid Badge Class"
                                              validate={{
                                                required: { value: true },
                                              }}
                                              value={orderList.badgeclass || "danger"}
                                            >
                                              <option value="success" key="pending">Green</option>
                                              <option value="danger" key="partial">Red</option>
                                              <option value="warning" key="fulfilled">Yellow</option>
                                            </AvField>
                                          </div>

                                          <div className="mb-3">
                                            <AvField
                                              name="note"
                                              label="Note"
                                              type="text"
                                              errorMessage="Invalid Note"
                                              validate={{
                                                required: { value: false },
                                              }}
                                              value={orderList.note || ""}
                                            />
                                          </div>
                                          
                                        </Col>
                                      </Row>
                                      <Row>
                                        <Col>
                                          <div className="text-end">
                                            <button
                                              type="submit"
                                              className="btn btn-success save-user"
                                            >
                                              Save
                                            </button>
                                          </div>
                                        </Col>
                                      </Row>
                                    </AvForm>
                                  </ModalBody>
                                </Modal>
                              </Col>
                            </Row>
                            <Row className="align-items-md-center mt-30">
                              <Col className="pagination pagination-rounded justify-content-end mb-2 inner-custom-pagination">
                                <PaginationListStandalone
                                  {...paginationProps}
                                />
                              </Col>
                            </Row>
                          </React.Fragment>
                        )}
                      </ToolkitProvider>
                    )}
                  </PaginationProvider>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  );
};

Orders.propTypes = {
  orders: PropTypes.array,
  onGetOrders: PropTypes.func,
  onAddNewOrder: PropTypes.func,
  onDeleteOrder: PropTypes.func,
  onUpdateOrder: PropTypes.func,
};

export default withRouter(Orders);
