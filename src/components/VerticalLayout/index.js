import PropTypes from "prop-types"
import React, { useEffect } from "react"

import { withRouter } from "react-router-dom"
import {
  changeLayout,
  changeSidebarStyle,
  changeSidebarStyleImage,
  changeSidebarType,
  changeTopbarStyle,
  changeLayoutWidth,
} from "../../store/actions"

// Layout Related Components
import Header from "./Header"
import Sidebar from "./Sidebar"
import Footer from "./Footer"
import Rightbar from "../CommonForBoth/RightSidebar"

//redux
import { useSelector, useDispatch } from "react-redux"

const Layout = props => {
  const dispatch = useDispatch()

  const {
    isPreloader,
    leftSideBarStyleImage,
    layoutWidth,
    leftSideBarType,
    topbarStyle,
    showRightSidebar,
    leftSideBarStyle,
  } = useSelector(state => ({
    isPreloader: state.Layout.isPreloader,
    leftSideBarStyleImage: state.Layout.leftSideBarStyleImage,
    leftSideBarType: state.Layout.leftSideBarType,
    layoutWidth: state.Layout.layoutWidth,
    topbarStyle: state.Layout.topbarStyle,
    showRightSidebar: state.Layout.showRightSidebar,
    leftSideBarStyle: state.Layout.leftSideBarStyle,
  }))

  const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent)

  const toggleMenuCallback = () => {
    if (leftSideBarType === "default") {
      dispatch(changeSidebarType("condensed", isMobile))
    } else if (leftSideBarType === "condensed") {
      dispatch(changeSidebarType("default", isMobile))
    }
  }

  /*
  layout  settings
  */

  useEffect(() => {
    if (isPreloader === true) {
      document.getElementById("preloader").style.display = "block"
      document.getElementById("status").style.display = "block"

      setTimeout(function () {
        document.getElementById("preloader").style.display = "none"
        document.getElementById("status").style.display = "none"
      }, 2500)
    } else {
      document.getElementById("preloader").style.display = "none"
      document.getElementById("status").style.display = "none"
    }
  }, [isPreloader])

  useEffect(() => {
    window.scrollTo(0, 0)
  }, []);

  useEffect(() => {
    dispatch(changeLayout("vertical"));
  }, [dispatch]);

  useEffect(() => {
    if (leftSideBarStyle) {
      dispatch(changeSidebarStyle(leftSideBarStyle))
    }
  }, [leftSideBarStyle, dispatch])

  useEffect(() => {
    if (leftSideBarStyleImage) {
      dispatch(changeSidebarStyleImage(leftSideBarStyleImage))
    }
  }, [leftSideBarStyleImage, dispatch])

  useEffect(() => {
    if (layoutWidth) {
      dispatch(changeLayoutWidth(layoutWidth))
    }
  }, [layoutWidth, dispatch])

  useEffect(() => {
    if (leftSideBarType) {
      dispatch(changeSidebarType(leftSideBarType))
    }
  }, [leftSideBarType, dispatch])

  useEffect(() => {
    if (topbarStyle) {
      dispatch(changeTopbarStyle(topbarStyle))
    }
  }, [topbarStyle, dispatch])

  return (
    <React.Fragment>
      <div id="preloader">
        <div id="status">
          <div className="spinner-chase">
            <div className="chase-dot" />
            <div className="chase-dot" />
            <div className="chase-dot" />
            <div className="chase-dot" />
            <div className="chase-dot" />
            <div className="chase-dot" />
          </div>
        </div>
      </div>

      <div id="layout-wrapper">
        <Header toggleMenuCallback={toggleMenuCallback} />
        <Sidebar
          style={leftSideBarStyle}
          type={leftSideBarType}
          isMobile={isMobile}
        />
        <div className="main-content">{props.children}</div>
        <Footer />
      </div>
      {showRightSidebar ? <Rightbar /> : null}
    </React.Fragment>
  )
}

Layout.propTypes = {
  changeLayoutWidth: PropTypes.func,
  changeSidebarStyle: PropTypes.func,
  changeSidebarStyleImage: PropTypes.func,
  changeSidebarType: PropTypes.func,
  changeTopbarStyle: PropTypes.func,
  children: PropTypes.object,
  isPreloader: PropTypes.any,
  layoutWidth: PropTypes.any,
  leftSideBarStyle: PropTypes.any,
  leftSideBarStyleImage: PropTypes.any,
  leftSideBarType: PropTypes.any,
  location: PropTypes.object,
  showRightSidebar: PropTypes.any,
  topbarStyle: PropTypes.any,
}

export default withRouter(Layout)
