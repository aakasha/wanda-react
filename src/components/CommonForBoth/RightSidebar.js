import React from "react"
import PropTypes from "prop-types"
import { Row, Col, FormGroup } from "reactstrap"

import { connect } from "react-redux"
import {
  changeLayout,
  changeLayoutWidth,
  changeSidebarStyle,
  changeSidebarStyleImage,
  changeSidebarType,
  changePreloader,
  changeTopbarStyle,
  showRightSidebarAction,
} from "../../store/actions"

//SimpleBar
import SimpleBar from "simplebar-react"

import { Link } from "react-router-dom"

import "../../components/CommonForBoth/rightbar.scss"


//constants
import {
  layoutTypes,
  layoutWidthTypes,
  topBarStyleTypes,
  leftSidebarTypes,
  leftSideBarStyleTypes,
} from "../../constants/layout"

const RightSidebar = props => {
  const onCloseRightBar = () => {
    const { onClose } = props
    if (onClose) {
      onClose()
    }
  }
  return (
    <React.Fragment>
      <SimpleBar style={{ height: "900px" }}>
        <div data-simplebar className="h-100">
          <div className="rightbar-title px-3 py-4">
            <Link
              to="#"
              onClick={e => {
                onCloseRightBar();
              }}
              className="right-bar-toggle float-end"
            >
              <i className="mdi mdi-close noti-icon" />
            </Link>
            <h5 className="m-0">Settings</h5>
          </div>

          <hr className="my-0" />

          <div className="p-4">
            <div className="radio-toolbar">
              <span className="mb-2 d-block">Layouts</span>
              <input
                type="radio"
                id="radioVertical"
                name="radioFruit"
                value={layoutTypes.VERTICAL}
                checked={props.layoutType === layoutTypes.VERTICAL}
                onChange={e => {
                  if (e.target.checked) {
                    props.changeLayout(e.target.value)
                  }
                }}
              />
              <label className="me-1" htmlFor="radioVertical">Vertical</label>
              <input
                type="radio"
                id="radioHorizontal"
                name="radioFruit"
                value={layoutTypes.HORIZONTAL}
                checked={props.layoutType === layoutTypes.HORIZONTAL}
                onChange={e => {
                  if (e.target.checked) {
                    props.changeLayout(e.target.value)
                  }
                }}
              />
              <label htmlFor="radioHorizontal">Horizontal</label>
            </div>

            <hr className="mt-1" />

            <div className="radio-toolbar">
              <span className="mb-2 d-block" id="radio-title">
                Layout Width
              </span>
              <input
                type="radio"
                id="radioFluid"
                name="radioWidth"
                value={layoutWidthTypes.FLUID}
                checked={props.layoutWidth === layoutWidthTypes.FLUID}
                onChange={e => {
                  if (e.target.checked) {
                    props.changeLayoutWidth(e.target.value)
                  }
                }}
              />
              <label className="me-1" htmlFor="radioFluid">Fluid</label>
              <input
                type="radio"
                id="radioBoxed"
                name="radioWidth"
                value={layoutWidthTypes.BOXED}
                checked={props.layoutWidth === layoutWidthTypes.BOXED}
                onChange={e => {
                  if (e.target.checked) {
                    props.changeLayoutWidth(e.target.value)
                  }
                }}
              />
              <label htmlFor="radioBoxed" className="me-2">
                Boxed
              </label>
              <input
                type="radio"
                id="radioscrollable"
                name="radioscrollable"
                value={layoutWidthTypes.SCROLLABLE}
                checked={props.layoutWidth === layoutWidthTypes.SCROLLABLE}
                onChange={e => {
                  if (e.target.checked) {
                    props.changeLayoutWidth(e.target.value)
                  }
                }}
              />
              <label htmlFor="radioscrollable">Scrollable</label>
            </div>
            <hr className="mt-1" />

            <div className="radio-toolbar">
              <span className="mb-2 d-block" id="radio-title">
                Topbar Style
              </span>
              <input
                type="radio"
                id="radioStyleLight"
                name="radioStyle"
                value={topBarStyleTypes.LIGHT}
                checked={props.topbarStyle === topBarStyleTypes.LIGHT}
                onChange={e => {
                  if (e.target.checked) {
                    props.changeTopbarStyle(e.target.value)
                  }
                }}
              />
              <label className="me-1" htmlFor="radioStyleLight">Light</label>
              <input
                type="radio"
                id="radioStyleDark"
                name="radioStyle"
                value={topBarStyleTypes.DARK}
                checked={props.topbarStyle === topBarStyleTypes.DARK}
                onChange={e => {
                  if (e.target.checked) {
                    props.changeTopbarStyle(e.target.value)
                  }
                }}
              />
              <label className="me-1" htmlFor="radioStyleDark">Dark</label>
              {props.layoutType === "vertical" ? null : (
                <>
                  <input
                    type="radio"
                    id="radioStyleColored"
                    name="radioStyle"
                    value={topBarStyleTypes.COLORED}
                    checked={props.topbarStyle === topBarStyleTypes.COLORED}
                    onChange={e => {
                      if (e.target.checked) {
                        props.changeTopbarStyle(e.target.value)
                      }
                    }}
                  />
                  <label className="me-1" htmlFor="radioStyleColored">Colored</label>{" "}
                </>
              )}
            </div>

            {props.layoutType === "vertical" ? (
              <React.Fragment>
                <hr className="mt-1" />
                <div className="radio-toolbar">
                  <span className="mb-2 d-block" id="radio-title">
                    Left Sidebar Type{" "}
                  </span>
                  <input
                    type="radio"
                    id="sidebarDefault"
                    name="sidebarType"
                    value={leftSidebarTypes.DEFAULT}
                    checked={props.leftSideBarType === leftSidebarTypes.DEFAULT}
                    onChange={e => {
                      if (e.target.checked) {
                        props.changeSidebarType(e.target.value)
                      }
                    }}
                  />
                  <label className="me-1" htmlFor="sidebarDefault">Default</label>
                  <input
                    type="radio"
                    id="sidebarCompact"
                    name="sidebarType"
                    value={leftSidebarTypes.COMPACT}
                    checked={props.leftSideBarType === leftSidebarTypes.COMPACT}
                    onChange={e => {
                      if (e.target.checked) {
                        props.changeSidebarType(e.target.value)
                      }
                    }}
                  />
                  <label className="me-1" htmlFor="sidebarCompact">Compact</label>
                  <input
                    type="radio"
                    id="sidebarIcon"
                    name="sidebarType"
                    value={leftSidebarTypes.ICON}
                    checked={props.leftSideBarType === leftSidebarTypes.ICON}
                    onChange={e => {
                      if (e.target.checked) {
                        props.changeSidebarType(e.target.value)
                      }
                    }}
                  />
                  <label className="me-1" htmlFor="sidebarIcon">Icon</label>
                </div>

                <hr className="mt-1" />

                <div className="radio-toolbar coloropt-radio">
                  <span className="mb-2 d-block" id="radio-title">
                    Left Sidebar Color Options
                  </span>
                  <Row>
                    <Col>
                      <input
                        type="radio"
                        id="leftsidebarStylelight"
                        name="leftsidebarStyle"
                        value={leftSideBarStyleTypes.LIGHT}
                        checked={props.leftSideBarStyle === leftSideBarStyleTypes.LIGHT}
                        onChange={e => {
                          if (e.target.checked) {
                            props.changeSidebarStyle(e.target.value)
                          }
                        }}
                      />
                      <label
                        htmlFor="leftsidebarStylelight"
                        className="bg-light rounded-circle wh-30 me-1"
                      ></label>

                      <input
                        type="radio"
                        id="leftsidebarStyledark"
                        name="leftsidebarStyle"
                        value={leftSideBarStyleTypes.DARK}
                        checked={props.leftSideBarStyle === leftSideBarStyleTypes.DARK}
                        onChange={e => {
                          if (e.target.checked) {
                            props.changeSidebarStyle(e.target.value)
                          }
                        }}
                      />
                      <label
                        htmlFor="leftsidebarStyledark"
                        className="bg-dark rounded-circle wh-30 me-1"
                      ></label>

                      <input
                        type="radio"
                        id="leftsidebarStylecolored"
                        name="leftsidebarStyle"
                        value={leftSideBarStyleTypes.COLORED}
                        checked={props.leftSideBarStyle === leftSideBarStyleTypes.COLORED}
                        onChange={e => {
                          if (e.target.checked) {
                            props.changeSidebarStyle(e.target.value)
                          }
                        }}
                      />
                      <label
                        htmlFor="leftsidebarStylecolored"
                        className="bg-colored rounded-circle wh-30 me-1"
                      ></label>
                    </Col>
                  </Row>
                </div>
                <hr className="mt-1" />
              </React.Fragment>
            ) : null}

            <FormGroup>
              <span className="mb-2 d-block" id="radio-title">
                Preloader
              </span>

              <div className="form-check form-switch">
                <input
                  type="checkbox"
                  className="form-check-input checkbox"
                  id="checkbox_1"
                  checked={props.isPreloader}
                  onChange={() => {
                    props.changePreloader(!props.isPreloader)
                  }}
                />

                <label className="form-check-label" htmlFor="checkbox_1">
                  Preloader
                </label>
              </div>
            </FormGroup>

          </div>
        </div>
      </SimpleBar>
    </React.Fragment>
  )
}

RightSidebar.propTypes = {
  changeLayout: PropTypes.func,
  changeLayoutWidth: PropTypes.func,
  changePreloader: PropTypes.func,
  changeSidebarStyle: PropTypes.func,
  changeSidebarStyleImage: PropTypes.func,
  changeSidebarType: PropTypes.func,
  changeTopbarStyle: PropTypes.func,
  isPreloader: PropTypes.any,
  layoutType: PropTypes.any,
  layoutWidth: PropTypes.any,
  leftSideBarStyle: PropTypes.any,
  leftSideBarStyleImage: PropTypes.any,
  leftSideBarType: PropTypes.any,
  showRightSidebarAction: PropTypes.func,
  topbarStyle: PropTypes.any,
  onClose: PropTypes.func,
}

const mapStateToProps = state => {
  return { ...state.Layout }
}

export default connect(mapStateToProps, {
  changeLayout,
  changeSidebarStyle,
  changeSidebarStyleImage,
  changeSidebarType,
  changeLayoutWidth,
  changeTopbarStyle,
  changePreloader,
  showRightSidebarAction,
})(RightSidebar)
