// @flow
import {
  CHANGE_LAYOUT,
  CHANGE_LAYOUT_WIDTH,
  CHANGE_SIDEBAR_style,
  CHANGE_SIDEBAR_TYPE,
  CHANGE_TOPBAR_style,
  SHOW_RIGHT_SIDEBAR,
  CHANGE_SIDEBAR_style_IMAGE,
  CHANGE_PRELOADER,
  TOGGLE_LEFTMENU,
  SHOW_SIDEBAR,
} from "./actionTypes"

//constants
import {
  layoutTypes,
  layoutWidthTypes,
  topBarStyleTypes,
  leftSidebarTypes,
  leftSideBarStyleTypes,
} from "../../constants/layout";

const INIT_STATE = {
  layoutType: layoutTypes.HORIZONTAL,
  layoutWidth: layoutWidthTypes.FLUID,
  leftSideBarStyle: leftSideBarStyleTypes.DARK,
  leftSideBarType: leftSidebarTypes.DEFAULT,
  topbarStyle: topBarStyleTypes.LIGHT,
  isPreloader: false,
  showRightSidebar: false,
  isMobile: false,
  showSidebar: true,
  leftMenu: false,
}

const Layout = (state = INIT_STATE, action) => {
  switch (action.type) {
    case CHANGE_LAYOUT:
      return {
        ...state,
        layoutType: action.payload,
      }
    case CHANGE_PRELOADER:
      return {
        ...state,
        isPreloader: action.payload,
      }

    case CHANGE_LAYOUT_WIDTH:
      return {
        ...state,
        layoutWidth: action.payload,
      }
    case CHANGE_SIDEBAR_style:
      return {
        ...state,
        leftSideBarStyle: action.payload,
      }
    case CHANGE_SIDEBAR_style_IMAGE:
      return {
        ...state,
        leftSideBarStyleImage: action.payload,
      }
    case CHANGE_SIDEBAR_TYPE:
      return {
        ...state,
        leftSideBarType: action.payload.sidebarType,
      }
    case CHANGE_TOPBAR_style:
      return {
        ...state,
        topbarStyle: action.payload,
      }
    case SHOW_RIGHT_SIDEBAR:
      return {
        ...state,
        showRightSidebar: action.payload,
      }
    case SHOW_SIDEBAR:
      return {
        ...state,
        showSidebar: action.payload,
      }
    case TOGGLE_LEFTMENU:
      return {
        ...state,
        leftMenu: action.payload,
      }

    default:
      return state
  }
}

export default Layout
