import { call, put, takeEvery } from "redux-saga/effects"

// Ecommerce Redux States
import {
  GET_ORDERS,
  ADD_NEW_ORDER,
  DELETE_ORDER,
  UPDATE_ORDER,

} from "./actionTypes"
import {
  getOrdersFail,
  getOrdersSuccess,
  addOrderFail,
  addOrderSuccess,
  updateOrderSuccess,
  updateOrderFail,
  deleteOrderSuccess,
  deleteOrderFail,

} from "./actions"

//Include Both Helper File with needed methods
import {
  getOrders,
  addNewOrder,
  updateOrder,
  deleteOrder,
} from "helpers/liveandmockbackend_helper"

function* fetchOrders() {
  try {
    const response = yield call(getOrders)
    yield put(getOrdersSuccess(response))
  } catch (error) {
    yield put(getOrdersFail(error))
  }
}

function* onUpdateOrder({ payload: order }) {
  try {
    const response = yield call(updateOrder, order)
    yield put(updateOrderSuccess(response))
  } catch (error) {
    yield put(updateOrderFail(error))
  }
}

function* onDeleteOrder({ payload: order }) {
  try {
    const response = yield call(deleteOrder, order)
    yield put(deleteOrderSuccess(response))
  } catch (error) {
    yield put(deleteOrderFail(error))
  }
}

function* onAddNewOrder({ payload: order }) {
  try {
    const response = yield call(addNewOrder, order)
    yield put(addOrderSuccess(response))
  } catch (error) {
    yield put(addOrderFail(error))
  }
}

function* orderSaga() {
  yield takeEvery(GET_ORDERS, fetchOrders)
  yield takeEvery(ADD_NEW_ORDER, onAddNewOrder)
  yield takeEvery(UPDATE_ORDER, onUpdateOrder)
  yield takeEvery(DELETE_ORDER, onDeleteOrder)
}

export default orderSaga
