
let orders = [
  {
    id: "e6d14fd0",
    orderdate: "2019-10-08",
    name: "Rahul",
    phone: "94480798",
    email: "rahulapoorva1@gmail.com",
    fromAddress: "19 Cantonment close",
    toAddress: "6 Kitchener Link",
    service: [
      "wrap",
      "pickup"
    ],
    movingDate: "2021-09-20",
    orderstatus: "pending", 
    badgeclass: "success",
    note: "the customer won’t be available Bll 12 pm", 
    createdAt: "2021-09-19T00:59:54.573Z",
    updatedAt: "2021-09-19T00:59:54.573Z"
  },
  {
    id: "saW8CCENw",
    orderdate: "2021-09-19",
    name: "Aakash Apoorv",
    phone: "92550999",
    email: "aakash@gmail.com",
    fromAddress: "Oslo",
    toAddress: "Singapore",
    service: [
        "wrap"
    ],
    movingDate: "2021-09-10",
    orderstatus: "pending", 
    badgeclass: "success",
    note: "",
    createdAt: "2021-09-19T23:57:23.719Z",
    updatedAt: "2021-09-19T23:57:23.719Z"
}
]

export {
  orders
}
