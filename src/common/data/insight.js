const janTopSellingData = [
    {
        name: "Pickup",
        desc: "Express pickup (same-day pickup)",
        value: 37
    },
    {
        name: "Packing",
        desc: "Packing items into boxes",
        value: 72
    },
    {
        name: "Wrapping",
        desc: "Wrapping large items",
        value: 54
    },
];

const decTopSellingData = [
    {
        name: "Pickup",
        desc: "Express pickup (same-day pickup)",
        value: 18
    },
    {
        name: "Packing",
        desc: "Packing items into boxes",
        value: 51
    },
    {
        name: "Wrapping",
        desc: "Wrapping large items",
        value: 24
    },
];

const novTopSellingData = [
    {
        name: "Pickup",
        desc: "Express pickup (same-day pickup)",
        value: 78
    },
    {
        name: "Packing",
        desc: "Packing items into boxes",
        value: 52
    },
    {
        name: "Wrapping",
        desc: "Wrapping large items",
        value: 25
    },
];

const octTopSellingData = [
    {
        name: "Pickup",
        desc: "Express pickup (same-day pickup)",
        value: 13
    },
    {
        name: "Packing",
        desc: "Packing items into boxes",
        value: 43
    },
    {
        name: "Wrapping",
        desc: "Wrapping large items",
        value: 12
    },
];

const janEarningData = [
    31, 40, 36, 51, 49, 72, 69, 56, 68, 82, 68, 76, 31, 40, 36, 51, 49, 72, 69, 56, 68, 43, 68, 18, 69, 56, 68, 82, 68, 76
];

const decEarningData = [
    42, 19, 32, 51, 49, 44, 14, 56, 68, 82, 68, 76, 42, 19, 32, 51, 49, 44, 14, 56, 68, 35, 68, 12, 14, 56, 68, 82, 68, 76
];

const novEarningData = [
    31, 40, 36, 51, 49, 72, 69, 12, 35, 42, 18, 76, 31, 40, 36, 51, 49, 72, 69, 12, 35, 42, 18, 5, 69, 12, 35, 42, 18, 76
];

const octEarningData = [
    31, 40, 49, 44, 14, 56, 69, 31, 40, 36, 51,  31, 40, 49, 44, 14, 56, 69, 31, 40, 36, 51, 14, 56, 8, 31, 40, 36, 51, 69
];

const chatData = [
    {
        id: 1,
        name: "Yves Hwang",
        msg: "Hi, can someone take a at order #yt6731",
        time: "10.00",
        isSender: false
    },
    {
        id: 2,
        name: "Aakash Apoorv",
        msg: "I'm following it, driver will pickup in 10 mins",
        time: "10.02",
        isSender: true
    },
    {
        id: 3,
        name: "Yves Hwang",
        msg: "Perfect",
        time: "10.06",
        isSender: false
    },
    {
        id: 4,
        name: "Yves Hwang",
        msg: "Can inform client about the order as well?",
        time: "10.06",
        isSender: false
    },
    {
        id: 5,
        name: "Aakash Apoorv",
        msg: "Yes",
        time: "10.07",
        isSender: true
    }
];

export {
    janTopSellingData,
    decTopSellingData,
    novTopSellingData,
    octTopSellingData,
    janEarningData,
    decEarningData,
    novEarningData,
    octEarningData,
    chatData
};