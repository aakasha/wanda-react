//REGISTER
export const POST_FAKE_REGISTER = "/post-fake-register"

//LOGIN
export const POST_FAKE_LOGIN = "/post-fake-login"
export const POST_FAKE_JWT_LOGIN = "/post-jwt-login"
export const POST_FAKE_PASSWORD_FORGET = "/fake-forget-pwd"
export const POST_FAKE_JWT_PASSWORD_FORGET = "/jwt-forget-pwd"
export const SOCIAL_LOGIN = "/social-login"

//PROFILE
export const POST_EDIT_JWT_PROFILE = "/post-jwt-profile"
export const POST_EDIT_PROFILE = "/post-fake-profile"

//ORDERS
export const GET_ORDERS = "/orders"
export const ADD_NEW_ORDER = "/add/order"
export const UPDATE_ORDER = "/update/order"
export const DELETE_ORDER = "/delete/order"

//ORDERS LIVE
const LIVE_BASE_URL = "https://cf3osjphd3.execute-api.eu-north-1.amazonaws.com/dev/"
export const LIVE_GET_ORDERS = `${LIVE_BASE_URL}api/v1/orders/`
export const LIVE_ADD_NEW_ORDER =`${LIVE_BASE_URL}api/v1/orders/`
export const LIVE_UPDATE_ORDER = `${LIVE_BASE_URL}api/v1/orders/`
export const LIVE_DELETE_ORDER = `${LIVE_BASE_URL}api/v1/orders/`

export const TOP_SELLING_DATA = "/top-selling-data"

export const GET_EARNING_DATA = "/earning-charts-data"